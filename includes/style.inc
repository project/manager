<?php

function manager_style() {
  $content = "li#content-type-%TYPE {
  display: block;
  background: transparent url('%URL');
  padding-left: 40px;
}
";
  return $content;
}
